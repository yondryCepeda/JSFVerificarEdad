package EdadBean;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ManagedBean
@RequestScoped
public class EdadRequest extends HttpServlet implements Serializable{
	
	private int edad;

	public int getEdad() {
		return edad;
		
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "EdadRequest [edad=" + edad + "]";
	}
	
	public String verificarEdad(){
		
		if (edad >= 18){
			System.out.println("usted es mayor de edad");
			return "mayor";
			
		} else{
			System.out.println("usted es menor de edad");
			return "menor";
		}
	}
		
}
